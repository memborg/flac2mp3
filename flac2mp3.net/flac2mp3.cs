using System;
using System.IO;
using NAudio.Wave;
using NAudio.Lame;
using FlacBox;
using FlacLibSharp;
using TagLib;
using TagLib.Id3v2;
using TagLib.Flac;
using System.Threading;

namespace Memborg.Flac2MP3{

    public class Flac2MP3{

        static int counter = 0;
        static object counterLock = new object();

        public Flac2MP3(){
        }

        static void Main(string[] args){
            string dir = args[0];

            foreach(string myFile in Directory.EnumerateFiles(dir, "*.flac", SearchOption.AllDirectories)){
                ThreadPool.SetMaxThreads(4,4);
                ThreadPool.QueueUserWorkItem(new WaitCallback(Process), myFile);
            }
            bool running = true;
            Thread.Sleep(10000);
            while(running){
                if(counter == 0){
                    running = false;
                }
                Thread.Sleep(10000);
            }
        }
        private static void Process(object filename){
            lock(counterLock){
                counter++;
            }

            string myFile = (string)filename;
            try{
                using(TagLib.Flac.File test = new TagLib.Flac.File(myFile) ){
                    byte[] bytes = System.IO.File.ReadAllBytes(myFile);
                    MemoryStream memstream = new MemoryStream(bytes);

                    string flacfile = myFile.Replace(".FLAC.flac", ".flac");
                    string mp3file = flacfile.Replace(".flac", ".mp3");

                    bool success = false;
                    using(WaveOverFlacStream wav = new WaveOverFlacStream(new FlacReader(memstream, false))){
                        using(WaveFileReader reader = new WaveFileReader(wav)){
                            using(LameMP3FileWriter writer = new LameMP3FileWriter(mp3file, reader.WaveFormat, LAMEPreset.STANDARD)){
                                try{
                                    reader.CopyTo(writer);
                                    success = true;
                                }catch(Exception e){
                                    Console.WriteLine("File failed: " + myFile);
                                    Console.WriteLine(e.ToString());
                                    Console.WriteLine("------------------------------------------");
                                }
                            }
                        }
                    }

                    using(TagLib.File tagfile = TagLib.File.Create(mp3file)){
                        TagLib.Id3v2.Tag t = (TagLib.Id3v2.Tag)tagfile.GetTag(TagLib.TagTypes.Id3v2, true);
                        test.Tag.CopyTo(t, true);
                        tagfile.Save();
                    }

                    if(success){
                        System.IO.File.Delete(myFile);
                    }else{
                        System.IO.File.Delete(mp3file);
                    }
                }
            }catch(Exception e){
                Console.WriteLine("File failed: " + myFile);
                Console.WriteLine(e.ToString());
                Console.WriteLine("------------------------------------------");
            }

            lock(counterLock){
                counter--;
            }
        }
    }
}
